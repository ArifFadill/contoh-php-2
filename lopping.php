<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>Berlatih lopping Php</h1>
    <?php
    echo "<h3>contoh 1</h3>";
    echo "<h5>looping 1</h5>";
    for($i=2; $i<=20; $i+=2){
        echo $i . "- I Love PHP <br> ";
    }

    echo "<h5>lopping 2</h5>";
    for($a=20; $a>=2; $a-=2){
        echo $a . "- I Love PHP <br>";
    }

    echo "<h3>contoh 2</h3>";
    $number = [18, 45, 29, 61, 47, 34];
    echo "Array number : ";
    print_r($number);
    echo "<br>";
    echo "Hasil sisa dari array number";
    foreach($number as $value){
        $rest[]= $value %= 5;
    }
    print_r($rest);
    echo "<br>";


    echo "<h3>contoh 3</h3>";
    $items = [
        ['001', 'Keyboard Logitek', 60000, 'Keyboard yang mantap untuk kantoran', 'logitek.jpeg'], 
        ['002', 'Keyboard MSI', 300000, 'Keyboard gaming MSI mekanik', 'msi.jpeg'],
        ['003', 'Mouse Genius', 50000, 'Mouse Genius biar lebih pinter', 'genius.jpeg'],
        ['004', 'Mouse Jerry', 30000, 'Mouse yang disukai kucing', 'jerry.jpeg']
    ];
    foreach($items as $key => $value){
        $barang = array(
            "id" => $value[0],
            "name" => $value[1],
            "price" => $value[2],
            "description" => $value[3],
            "sourche" => $value[4]
        );
        print_r($barang);
        echo "<br>";
    }

    echo "<h3>Contoh 4</h3>";
    for($j=1; $j<=5; $j++){
        for($b=1; $b<=$j; $b++){
            echo "*";
        }
        echo "<br>";
    }
    

    ?>
</body>
</html>