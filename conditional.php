<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h3>Contoh Conditional<h/3>

    <?php
        echo "<h3> Soal No 1 Greetings </h3>";
        
        contoh: greetings("abduh");
        Output: "Halo Abduh, Selamat Datang di PKS Digital School!";
        
        
        function greetings($nama){
            echo "hallo ". $nama. " Selamat Datang di PKS Digital School!". "<br>";
        }
        
        
        
         greetings("Bagas");
         greetings("Wahyu");
         greetings("Abdul");
        
        echo "<br>";
        
        echo "<h3>Soal No 2 Reverse String</h3>";

        // Code function di sini
        function reverse($kata1){
            $panjangkata = strlen($kata1);
            $tampung = "";
            for($i=($panjangkata- 1) ; $i>=0; $i--){
                $tampung .= $kata1[$i];
            }
            return $tampung;
        }

        function reverseString($kata2){
            $string=reverse($kata2);
            echo $string ."<br>";
        }


        // Hapus komentar di bawah ini untuk jalankan Code
        reverseString("abduh");
        reverseString("Digital School");
        reverseString("We Are PKS Digital School Developers");
        echo "<br>";


        echo "<h3>Soal No 3 Palindrome </h3>";


            // Code function di sini
            function palindrome($kata3){
                $balik = reverse($kata3);
                if($kata3 === $balik){
                    echo "true <br>";
                }else{
                    echo "false <br>";
                }
            }

            // Hapus komentar di bawah ini untuk jalankan code
            palindrome("civic") ; // true
            palindrome("nababan") ; // true
            palindrome("jambaban"); // false
            palindrome("racecar"); // true



        echo "<h3>Soal No 4 Tentukan Nilai </h3>";
            

            // Code function di sini
            function tentukan_nilai($angka){
                $output = "";
                if($angka>=85 && $angka<100){
                    $output .= "sangat baik";
                }else if($angka >=70 && $angka<85){
                    $output .= "baik";
                }else if($angka >=60 && $angka<70){
                    $output .= "cukup";
                }else if($angka >=43 && $angka<60){
                    $output .= "kurang";
                }
                return $output . "<br>";
            }
            


            // Hapus komentar di bawah ini untuk jalankan code
            echo tentukan_nilai(98); //Sangat Baik
            echo tentukan_nilai(76); //Baik
            echo tentukan_nilai(67); //Cukup
            echo tentukan_nilai(43); //Kurang
            echo "<br>";


    ?>
    
</body>
</html>